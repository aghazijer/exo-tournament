package com.nespresso.sofa.recruitement.tournament.fighter;


public class Swordsman extends Fighter {
	
	

	private static final int DAMAGE = 5;
	private static final int HIT_POINTS = 100;

	public Swordsman(String string) {
		super();
	}

	public Swordsman() {
		super();
		this.hitPoints = HIT_POINTS;
		this.damage = DAMAGE;
	}


	public Swordsman equip(String equip) {
		this.equipments.add(equip);
		return this;
	}

}
