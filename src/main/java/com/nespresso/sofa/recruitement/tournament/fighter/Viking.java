package com.nespresso.sofa.recruitement.tournament.fighter;


public class Viking extends Fighter {
	
	private static final int DAMAGE = 6;
	private static final int HIT_POINTS = 120;

	public Viking() {
		super();
		this.hitPoints = HIT_POINTS;
		this.damage = DAMAGE;
	}
	
	public Viking equip(String equip) {
		this.equipments.add(equip);
		return this;
	}


}
