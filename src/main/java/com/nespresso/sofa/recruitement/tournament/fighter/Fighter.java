package com.nespresso.sofa.recruitement.tournament.fighter;

import java.util.ArrayList;
import java.util.List;

public class Fighter {
	
	private static final String BUCKLER = "buckler";
	private static final String ARMOR = "armor";
	private static final int ARMOR_RECEIVED_DAMAGES_REDUCE = 3;
	private static final int ARMOR_DELIVERED_DAMAGES_REDUCE = 1;
	
	protected int hitPoints;
	protected int damage;
	protected List<String> equipments;
	private int recivedHits;
	private int blockedHits;
	private int delivredHits;
	
	
	public Fighter() {
		this.recivedHits = 0;
		this.blockedHits = 0;
		this.delivredHits = 0;
		this.equipments = new ArrayList<>();
	}
	
	
	public void engage(Fighter opponent) {
		if(this.hitPoints != 0) {
			if(this.canAttack()){
				opponent.receiveHit(this.damage(), getType());
			}
			this.delivredHits ++;
			opponent.engage(this);
		}
		else {
			return;
		}
	}


	private boolean hasArmor() {
		return this.equipments.contains(ARMOR);
	}


	private int damage() {
		if(this.hasArmor()) {
			return this.damage - ARMOR_DELIVERED_DAMAGES_REDUCE;
		}
		return this.damage;
	}


	private String getType() {
		return this.getClass().getName();
	}


	private void receiveHit(int damage, String opponentType) {
		if(this.canHasDamage(opponentType)){
			int receivedDamage = receivedDamage(damage);
			if(this.hitPoints >= receivedDamage) {
				this.hitPoints = this.hitPoints - receivedDamage;
			}
			else {
				this.hitPoints = 0;
			}
		}
		this.recivedHits ++;
	}

	private int receivedDamage(int damage2) {
		return damage - blockingArmorDamage();
	}


	private int blockingArmorDamage() {
		if(this.hasArmor())
		{
			return ARMOR_RECEIVED_DAMAGES_REDUCE;
		}
		return 0;
	}


	private boolean canHasDamage(String opponentType) {
		if(canBlock(opponentType))
		{
			this.blockedHits ++;
			return false;
		}
		return true;
	}


	private boolean canAttack() {
		return (!Highlander.class.getName().equals(getType())) || (this.delivredHits % 3 != 2);
	}


	private boolean canBlock(String opponentType) {
		return (equipments.contains(BUCKLER) && this.recivedHits % 2 == 0)
				&& (!Viking.class.getName().equals(opponentType) || (blockedHits <= 3));
	}


	public int hitPoints() {
		return this.hitPoints;
	}
	
	

}
